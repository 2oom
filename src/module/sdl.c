/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#define SDL_MODULE "SDL"
#define SDL_MODULE_VERSION "0.1.0"

#include <stdlib.h>
#include <limits.h>
#include <SDL.h>

#include "../av/avfile.h"
#include "../plugin/plugin.h"
#include "../sys/log.h"

#define init sdl_LTX_init

static struct log_context logger;
static struct av_accessor av;

static struct SDL_AudioSpec sdlfmt;
static struct audio_format fmt;

struct audio_data {
	int    flags;
	int    playing;
	int    delete;
	int    aid;
	Uint8 *data;
	size_t datasz;
};

static struct channel {
	double volume;     /**< Current channel volume. */

	double voltarget;  /**< Target channel volume. */
	double fadeamt;    /**< Amount to adjust volume each sample. */

	int chanid;
	struct audio_data *data;
	int nloops;
} *channels;
static int nchannels;

static unsigned int opensounds;

/* Sets a channel to fade to the target volume over the course of nsec seconds.
 */
static void setfade(int c, double target, double nsec)
{
	SDL_LockAudio();

	if (target == channels[c].volume)
		return (void) (channels[c].fadeamt = 0);

	channels[c].voltarget = target;
	channels[c].fadeamt = (target-channels[c].volume)/(nsec*sdlfmt.freq);
	
	SDL_UnlockAudio();
}

/* Delete opened audio data */
static void data_delete(struct audio_data *data)
{
	free(data->data);
	free(data);
	opensounds--;
}

/* Stop a channel */
static inline void stop(int c)
{
	channels[c].data->playing--;
	if (channels[c].data->delete && !channels[c].data->playing)
		data_delete(channels[c].data);
	channels[c].data = NULL;
}

/* Apply one sample's worth of fading to channel c. */
static inline void fade(int c)
{
	if (channels[c].fadeamt == 0)
		return;
	else if (channels[c].fadeamt > 0) {
		channels[c].volume += channels[c].fadeamt;
		if (channels[c].volume > channels[c].voltarget) {
			channels[c].volume = channels[c].voltarget;
			channels[c].fadeamt = 0;
		}
	} else if (channels[c].fadeamt < 0) {
		channels[c].volume += channels[c].fadeamt;
		if (channels[c].volume < channels[c].voltarget) {
			if (channels[c].voltarget == 0)
				stop(c);

			channels[c].volume = channels[c].voltarget;
			channels[c].fadeamt = 0;
		}
	}
}

static int setchannels(int numchannels)
{
	int i;
	struct channel *new;

	SDL_LockAudio();
	if (!(new = realloc(channels, numchannels * sizeof *channels))) {
		SDL_UnlockAudio();
		return -1;
	}
	
	channels = new;
	
	for (i = nchannels; i < numchannels; i++)
		channels[i].data = NULL;
	
	nchannels = numchannels;

	SDL_UnlockAudio();
	return 0;
}

/* Read a block of channel data */
static void stream_read(int c, int len)
{
	Uint8 *stream = channels[c].data->data;
	int rc;

	while (len > 0) {
		rc = av.read_audio(channels[c].data->aid, stream, len);
		if (rc == -1)
			break;
		
		if (rc < len && channels[c].nloops != 0) {
			if (av.rewind(channels[c].data->aid) == -1)
				break;
			if (channels[c].nloops > 0)
				channels[c].nloops--;
		} else if (rc < len)
			break; 

		stream += rc;
		len -= rc;
	}

	if (len > 0)
		memset(stream, 0, len);			
}

/* Audio callback */
static void render_audio(void *userdata, Uint8 *stream, int len)
{
	int i;
	Sint16 *samp, *buf;

	/* Read data for all channels */
	for (i = 0; i < nchannels; i++)
		if (channels[i].data && channels[i].data->flags & AUD_OPEN_STREAM)
			stream_read(i, len);

	/* TO DO: Not explode with sample formats other than S16_LE */
	for (samp = (Sint16 *) stream; (Uint8 *) samp - stream < len; samp++) {
		double m = 0, c;
		ptrdiff_t off = samp - (Sint16 *) stream;
		
		for (i = 0; i < nchannels; i++) {
			if (!channels[i].data)
				continue;

			buf = (Sint16 *) channels[i].data->data;
			c = channels[i].volume * (double)buf[off];

			fade(i);

			/* Mix sample into output */
			if (m >= 0 && c >= 0)
				m = (m + c) - (m * c) / 32768;
			else if (m <= 0 && c <= 0)
				m = (m + c) + (m * c) / 32768;
			else
				m = (m + c);
		}
		
		/* Render sample */
		*samp = m;
	}

	/* Clean up finished channels */
	for (i = 0; i < nchannels; i++)
		if (channels[i].data && !channels[i].nloops)
			stop(i);
}

static struct audio_data *sdl_ao_open(const char *resource, int flags)
{
	struct audio_data *new = calloc(1, sizeof *new);

	if (!new)
		goto err;

	if ((new->aid = av.open(resource, AV_INPUT_AUDIO, &fmt, NULL)) == -1)
		goto err;

	new->flags = flags;
	if (flags & AUD_OPEN_STREAM) {
		new->datasz = sdlfmt.samples * 4;

		if (!(new->data = malloc(new->datasz)))
			goto err;
	} else if (flags & AUD_OPEN_SAMPLE) {
		goto err;
	}

	SDL_LockAudio();
	opensounds++;
	SDL_UnlockAudio();

	return new;	
err:
	free(new);
	return NULL;
}

static void sdl_ao_close(struct audio_data *data)
{
	SDL_LockAudio();

	data->delete = 1;

	if (!data->playing)
		data_delete(data);
	
	SDL_UnlockAudio();
}

static int setupchannel(int c, struct audio_data *data, int nloops)
{
	static unsigned int chanid;

	data->playing++;
	channels[c].data = data;
	channels[c].chanid = chanid;
	channels[c].nloops = nloops;

	chanid = (chanid + 1) % ((unsigned int) INT_MAX + 1);

	return channels[c].chanid;
}

static int sdl_ao_play(struct audio_data *data, int nloops, double volume)
{
	int i, rc;

	if (!data || nloops < -1 || nloops == 0)
		return -1;
	
	SDL_LockAudio();

	for (i = 0; i < nchannels; i++)
		if (!channels[i].data)
			break;

	/* Cancelling a currently playing channel may be better */
	if (i >= nchannels)
		return -1;

	rc = setupchannel(i, data, nloops);
	channels[i].volume  = volume;
	channels[i].fadeamt = 0;

	SDL_UnlockAudio();

	return rc;
}

static int sdl_ao_stop(int id)
{
	int i;

	SDL_LockAudio();

	for (i = 0; i < nchannels; i++) {
		if (channels[i].chanid == id && channels[i].data) {
			stop(i);
			break;
		}
	}
	
	SDL_UnlockAudio();

	return (i >= nchannels) ? -1 : 0;
}

static int sdl_ao_fade(int id, struct audio_data *data, int nloops, double vol)
{
	int i, rc, chan = -1;

	SDL_LockAudio();

	/* Find some channels */
	for (i = 0; i < nchannels; i++) {
		if (channels[i].chanid == id)
			setfade(i, 0, 4);

		if (!channels[i].data)
			chan = i;
	}

	if (chan == -1)
		return -1;

	rc = setupchannel(chan, data, nloops);
	channels[chan].volume = 0;
	setfade(chan, vol, 4);
	
	SDL_UnlockAudio();

	return rc;
}

/* Open the SDL audio device.  TO DO:  This is ugly, fix it. */
static int sdl_ao_init(int flags)
{
	struct SDL_AudioSpec desired;

	if (!SDL_WasInit(SDL_INIT_AUDIO))
		if (SDL_InitSubSystem(SDL_INIT_AUDIO) == -1) {
			logger.write(LOG_ERROR, SDL_MODULE,
			            "Failed to init audio: %s", SDL_GetError());
			return -1;
		}
	
	/* TO DO: This should be configurable */
	desired.freq     = 22050;
	desired.format   = AUDIO_S16LSB;
	desired.channels = 2;
	desired.userdata = NULL;
	desired.callback = render_audio;
	desired.samples  = 2048;

	if (SDL_OpenAudio(&desired, &sdlfmt) == -1) {
		logger.write(LOG_ERROR, SDL_MODULE,
		             "Failed to open audio device: %s", SDL_GetError());
		return -1;
	}

	fmt.frequency = sdlfmt.freq;
	fmt.channels  = sdlfmt.channels;

	switch (sdlfmt.format) {
	case AUDIO_U8:
		fmt.format = AFMT_U8;
		break;
	case AUDIO_S8:
		fmt.format = AFMT_S8;
		break;
	case AUDIO_U16LSB:
		fmt.format = AFMT_U16_LE;
		break;
	case AUDIO_S16LSB:
		fmt.format = AFMT_S16_LE;
		break;
	case AUDIO_U16MSB:
		fmt.format = AFMT_U16_BE;
		break;
	case AUDIO_S16MSB:
		fmt.format = AFMT_S16_BE;
		break;
	default:
		goto err;
	}
	
	if (setchannels(8) == -1)
		goto err;

	SDL_PauseAudio(0);

	logger.write(LOG_INFO, SDL_MODULE,
	             "Audio output initialized successfully.");

	return 0;
err:
	SDL_CloseAudio();
	return -1;
}

static int sdl_ao_fini(int force)
{
	int i;

	SDL_LockAudio();

	/* IF our refcounts are OK, opensounds > 0 means nothing is playing. */
	if (!force && opensounds) {
		logger.write(LOG_WARNING, SDL_MODULE,
		             "Can't shutdown while sounds still open.");
		return -1;
	}

	SDL_UnlockAudio();
	SDL_CloseAudio();
	SDL_QuitSubSystem(SDL_INIT_AUDIO);

	for (i = 0; i < nchannels; i++)
		if (channels[i].data)
			stop(i);
	
	logger.write(LOG_INFO, SDL_MODULE, "%s audio shutdown successful.",
	                                   (force) ? "Forced" : "Normal");
	
	return 0;
}

int init(plugin_useservice_t useservice)
{
	static struct audio_output ao_sdl = {
		#undef init
		.init  = sdl_ao_init,
		.fini  = sdl_ao_fini,
		.open  = sdl_ao_open,
		.close = sdl_ao_close,
		.play  = sdl_ao_play,
		.stop  = sdl_ao_stop,
		.fade  = sdl_ao_fade,
	};

	if (useservice(LOG_SERVICE, SDL_MODULE, &logger))
		return -1;
	if (useservice(AV_ACCESS_SERVICE, SDL_MODULE, &av))
		return -1;
	if (useservice(AO_SERVICE, SDL_MODULE, &ao_sdl))
		return -1;
	
	if (SDL_Init(SDL_INIT_NOPARACHUTE) == -1) {
		logger.write(LOG_WARNING, SDL_MODULE,
		             "Failed to initialize SDL: %s", SDL_GetError());
		return -1;
	}

	logger.write(LOG_INFO, SDL_MODULE,
	             "SDL Audio/Video output driver " SDL_MODULE_VERSION);
	
	return 0;
}
