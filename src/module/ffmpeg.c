/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#define FFMPEG_MODULE "AV-FFmpeg"
#define FFMPEG_MOD_VERSION "0.1.0"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avstring.h>

#include "../plugin/plugin.h"
#include "../av/avfile.h"
#include "../vfs/vfs.h"
#include "../sys/log.h"

#define init ffmpeg_LTX_init

#define MIN(a,b) (((a)<(b))?(a):(b))

static struct resource {
	AVFormatContext *fmt;
	AVCodecContext *actx, *vctx;
	AVCodec *acodec, *vcodec;
	int astream, vstream;

	int16_t *aframe;
	int aframelen, aframeoff;

	struct audio_format afmt;
} *data;
static int datasz = 4;

static struct vfs_access vfs;
static struct log_context logger;

/* 2ooM VFS open handler for libavformat */
static int ff_vfs_open(URLContext *h, const char *filename, int flags)
{
	struct vfs_file *f;

	av_strstart(filename, "vfs:", &filename);

	/* 2ooM VFS is read-only */
	if (flags & URL_RDWR || flags & URL_WRONLY)
		return -EPERM;

	if (!(f = vfs.open(filename)))
		return -ENOENT;
	
	h->priv_data = f;

	return 0;
}

static int ff_vfs_read(URLContext *h, unsigned char *buf, int size)
{
	return vfs.read(h->priv_data, buf, size);
}

static int ff_vfs_write(URLContext *h, unsigned char *buf, int size)
{
	return -1; /* 2ooM VFS is read-only */
}

static offset_t ff_vfs_seek(URLContext *h, offset_t pos, int whence)
{
	return vfs.seek(h->priv_data, pos, whence);
}

static int ff_vfs_close(URLContext *h)
{
	vfs.close(h->priv_data);
	return 0;
}

static URLProtocol vfs_protocol = {
	.name      = "vfs",
	.url_open  = ff_vfs_open,
	.url_read  = ff_vfs_read,
	.url_write = ff_vfs_write,
	.url_seek  = ff_vfs_seek,
	.url_close = ff_vfs_close,
};

/* Double the size of the buffer for storing resources */
static int moreresources(void) {
	static int init;
	struct resource *new;
	int i;

	if (!(new = realloc(data, sizeof *data * datasz*2)))
		return -1;
	
	data = new;

	for (i = (!init) ? init = 1, 0 : datasz; i < datasz*2; i++)
		data[i].fmt = NULL;
	
	datasz *= 2;

	return 0;
}

/* Locate the first stream matching type, initialize codecs, and return the
 * stream ID.
 */
static int getstream(enum CodecType type, AVCodec **codec, AVFormatContext *fmt)
{
	AVCodecContext *ctx;
	int i;

	for (i = 0; i < fmt->nb_streams; i++)
		if (fmt->streams[i]->codec->codec_type == type)
			break;
	
	if (i >= fmt->nb_streams) {
		logger.write(LOG_DEBUG, FFMPEG_MODULE,
		             "Failed to find%s stream in %s",
			     (type == CODEC_TYPE_AUDIO) ? " audio" :
			     (type == CODEC_TYPE_VIDEO) ? " video" : "",
			     fmt->filename);
		return -1; 
	}
	
	ctx = fmt->streams[i]->codec;

	if (!(*codec = avcodec_find_decoder(ctx->codec_id))) {
		logger.write(LOG_DEBUG, FFMPEG_MODULE,
		             "Failed to find decoder for %s", ctx->codec_name);
		return -1;
	}

	if (avcodec_open(ctx, *codec) < 0) {
		logger.write(LOG_DEBUG, FFMPEG_MODULE,
		             "Failed to open decoder %s", (*codec)->name);
		return -1;
	}

	return i;
}

/* Attempt to open a VFS resource with FFmpeg */
static int ff_av_open(const char *resource, int type, 
                      const struct audio_format *afmt,
                      const struct video_format *vfmt)
{
	char *url;
	struct resource r = { .astream = -1, .vstream = -1 };
	int i;

	if (!(type & AV_INPUT_AUDIO || type & AV_INPUT_VIDEO))
		return -1;

	if (!(url = malloc(strlen("vfs:") + strlen(resource) + 1)))
		return -1;
	
	/* Find a free resource, or make one */
	for (i = 0; i < datasz; i++)
		if (data[i].fmt == NULL)
			break;
	if (i >= datasz)
		if (moreresources())
			goto err;
	
	strcpy(url, "vfs:");
	strcat(url, resource);

	if (av_open_input_file(&r.fmt, url, NULL, 0, NULL) != 0) {
		logger.write(LOG_DEBUG, FFMPEG_MODULE,
		             "Failed to open %s", url);
		goto err;
	}

	if (av_find_stream_info(r.fmt) < 0) {
		logger.write(LOG_DEBUG, FFMPEG_MODULE,
		             "Failed to find stream info for %s", url);
		goto err;
	}

	if (type & AV_INPUT_AUDIO) {
		r.astream = getstream(CODEC_TYPE_AUDIO, &r.acodec, r.fmt);
		if (r.astream == -1)
			goto err;
		r.actx = r.fmt->streams[r.astream]->codec;
		r.afmt = *afmt;
		if (!(r.aframe = malloc(AVCODEC_MAX_AUDIO_FRAME_SIZE)))
			goto err;
	}
	if (type & AV_INPUT_VIDEO) {
		r.vstream = getstream(CODEC_TYPE_VIDEO, &r.vcodec, r.fmt);
		if (r.vstream == -1)
			goto err;
		r.vctx = r.fmt->streams[r.vstream]->codec;
	}

	data[i] = r;
	return i;
err:
	if (r.actx)
		avcodec_close(r.actx);
	if (r.vctx)
		avcodec_close(r.vctx);
	if (r.fmt)
		av_close_input_file(r.fmt);
	
	free(r.aframe);

	free(url);
	return -1;
}

/* Close an open resource */
static void ff_av_close(int id)
{
	if (id >= datasz || !data[id].fmt)
		return;
	
	if (data[id].actx)
		avcodec_close(data[id].actx);
	if (data[id].vctx)
		avcodec_close(data[id].vctx);
	
	av_close_input_file(data[id].fmt);
	data[id].fmt = NULL;

	free(data[id].aframe);

	return;
}

/* Read decoded audio data */
static int ff_av_read_audio(int id, void *buf, int len)
{
	struct resource *d;
	int written = 0;
	char *stream = buf;

	if (id >= datasz || !data[id].fmt || data[id].astream == -1)
		return -1;
	
	d = &data[id];
	
	if (d->aframeoff < d->aframelen) {
		if (d->aframelen - d->aframeoff >= len) {
			memcpy(stream, d->aframe+(d->aframeoff/2), len);
			d->aframeoff += len;
			written += len;
			return written;
		} else {
			memcpy(stream, d->aframe+(d->aframeoff/2),
			               d->aframelen - d->aframeoff);

			written += d->aframelen - d->aframeoff;
			len     -= d->aframelen - d->aframeoff;
			stream  += d->aframelen - d->aframeoff;
		}
	}

	while (len > 0) {
		AVPacket packet;
		d->aframelen = 0, d->aframeoff = 0;
		if (av_read_frame(d->fmt, &packet) < 0)
			break;

		if (packet.stream_index == d->astream) {
			d->aframelen = AVCODEC_MAX_AUDIO_FRAME_SIZE;
			avcodec_decode_audio2(d->actx, d->aframe, &d->aframelen,
			                      packet.data, packet.size);

			if (d->aframelen <= 0)
				return av_free_packet(&packet), written;

			memcpy(stream, d->aframe, MIN(len, d->aframelen));

			d->aframeoff += MIN(len, d->aframelen);
			stream       += MIN(len, d->aframelen);
			written      += MIN(len, d->aframelen);
			len          -= MIN(len, d->aframelen);
		}

		av_free_packet(&packet);
	}

	return written;
}

static int ff_av_rewind(int id)
{
	if (id >= datasz || !data[id].fmt)
		return -1;

	if (av_seek_frame(data[id].fmt, -1, 0, AVSEEK_FLAG_BYTE) < 0)
		return -1;

	return 0;
}

int init(plugin_useservice_t useservice)
{
	static struct av_accessor av = {
		.open = ff_av_open,
		.close = ff_av_close,
		.read_audio = ff_av_read_audio,
		.rewind = ff_av_rewind,
	};

	if (useservice(LOG_SERVICE, FFMPEG_MODULE, &logger))
		return -1;

	if (useservice(VFSACCESS_SERVICE, FFMPEG_MODULE, &vfs))
		return -1;
	
	if (useservice(AV_PLUGIN_SERVICE, FFMPEG_MODULE, &av))
		return -1;
	
	if (moreresources())
		return -1;

	av_register_all();
	register_protocol(&vfs_protocol);

	logger.write(LOG_INFO, FFMPEG_MODULE,
	             "libavformat/libavcodec media input " FFMPEG_MOD_VERSION);
	
	return 0;
}


