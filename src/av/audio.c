/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <stdlib.h>

#include "audio.h"
#include "../sys/log.h"
#include "../plugin/plugin.h"

static struct audio_output *ao;

struct audio_data *ao_open(const char *resource, int flags)
{
	if (!ao)
		return NULL;
	
	return ao->open(resource, flags);
}

void ao_close(struct audio_data *data)
{
	if (!ao)
		return;
	
	return ao->close(data);
}

int ao_play(struct audio_data *data, int nloops, double volume)
{
	if (!ao)
		return -1;

	return ao->play(data, nloops, volume);
}

int ao_stop(int id)
{
	if (!ao)
		return -1;
	
	return ao->stop(id);
}

int ao_fade(int id, struct audio_data *data, int nloops, double volume)
{
	if (!ao)
		return -1;
	
	return ao->fade(id, data, nloops, volume);
}

int ao_init(int flags)
{
	if (!ao)
		return -1;

	return ao->init(flags);
}

int ao_fini(int force)
{
	if (!ao)
		return -1;
	
	return ao->fini(force);
}

static int ao_register(const char *name, void *data)
{
	ao = data;
	return 0;
}

int audio_init(void)
{
	if (plugin_addservice(AO_SERVICE, ao_register, NULL)) {
		log_write(LOG_ERROR, AO_MODULENAME,
		          "Failed to register service: %s", AO_SERVICE);
		return -1;
	}

	return 0;
}
