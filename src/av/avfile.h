/** @file avfile.h
 *  Prototypes for audio/video input plugins.
 *  An input plugin provides at least provisions for reading source data from
 *  the VFS, decoding it into one or more raw formats, and rewinding to the
 *  beginning.  Optional features include audio-video interleaving or seeking.
 */
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef AVFILE_H_
#define AVFILE_H_

#include "audio.h"

struct video_format;

#define AV_INPUT_AUDIO 1
#define AV_INPUT_VIDEO 2

#define AV_MODULENAME "AV"
#define AV_PLUGIN_SERVICE "AVPLUGIN"
#define AV_ACCESS_SERVICE "AVACCESS"

struct av_accessor {
	int  (*open)(const char *, int, const struct audio_format *,
	             const struct video_format *);
	void (*close)(int);
	int  (*read_audio)(int, void *, int);
	int  (*rewind)(int);
	struct av_accessor *next;
};

int av_init(void);

/** Attempt to open the specified VFS resource as media.
 * The specified resource will be opened by each input plugin in turn, until
 * one accepts the input or there are none left.  If it is accepted by a plugin,
 * the returned identifier can be used for accessing raw media data in the
 * specified formats.
 *
 * @see av_close
 * @param resource VFS resource to open.
 * @param type One or more AV_INPUT_* values, bitwise OR'd together.
 * @param afmt Audio format for raw data, if audio was requested.
 * @param vfmt Video format for raw data, if video was requested.
 * @return Identifier for use with further calls, or -1 on failure.
 */
int av_open(const char *resource, int type,
            const struct audio_format *afmt,
            const struct video_format *vfmt);

/** Free all resources associated with the provided identifier.
 *
 * @see av_open
 * @param id Identifier as returned by av_open()
 */
void av_close(int id);

/** Read up to len bytes of decoded audio data into buffer.
 * Decode audio data into the raw format associated with the provided
 * identifier, and store at most len bytes of complete samples into the buffer
 * pointed to by buf.
 *
 * @see av_open
 * @param id Identifier as returned by av_open().
 * @param buf Pointer to buffer of at least len bytes.
 * @param len Maximum number of bytes to write.
 * @return Number of bytes written, or -1 on failure.
 */
int av_read_audio(int id, void *buf, int len);

/** Rewind the media to the beginning.
 *
 * @param id Identifier as returned by av_open().
 * @return 0 on success, -1 on failure.
 */
int av_rewind(int id);

#endif
