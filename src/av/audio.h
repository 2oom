/** @file audio.h
 *  Declarations for audio facilities
*/
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#ifndef AUDIO_H_
#define AUDIO_H_

/**@defgroup Audio Audio Services
 * @{
 */

/** Plugin service identifier */
#define AO_SERVICE "AUDIO"

/** Module name for logging. */
#define AO_MODULENAME "audio"

/** Sample formats */
enum samplefmt {
	AFMT_DEF,    /**< Unspecified format */
	AFMT_U8,     /**< Unsigned 8-bit */
	AFMT_S8,     /**< Signed 8-bit */
	AFMT_U16_LE, /**< Signed 16-bit little-endian byte order */
	AFMT_U16_BE, /**< Unsigned 16-bit big-endian byte order */
	AFMT_S16_LE, /**< Signed 16-bit little-endian byte order */
	AFMT_S16_BE, /**< Signed 16-bit big-endian byte order */
};

/** Opaque type for storing AO plugin state */
struct audio_data;

/** Audio format specifier */
struct audio_format {
	int frequency;         /**< Number of samples per second */
	int channels;          /**< Number of channels in output */
	enum samplefmt format; /**< Format of a single sample */
};

struct audio_output {
	int (*init)(int);
	int (*fini)(int);
	struct audio_data *(*open)(const char *, int);
	void (*close)(struct audio_data *);
	int (*play)(struct audio_data *, int, double);
	int (*stop)(int);
	int (*fade)(int, struct audio_data *, int, double);
};

int audio_init(void);

/** Initialize audio output driver.
 *
 * @param flags Reserved.
 * @return 0 on successful initialization, -1 otherwise.
 */
int ao_init(int flags);

/** Shutdown audio output driver.
 * Shuts down the audio output driver if all open sounds are closed, and all
 * playback is halted.  Upon successful shutdown, the driver will be in a state
 * suitable for re-initialization by ao_init().
 * Shutdown may be forced, in which case the driver should attempt to halt even
 * if it is not currently in a sane state to do so- i.e. open or playing sounds.
 * In this case, it is unspecified as to whether open sounds are closed, or
 * playback halted.  Even if forced, a shutdown is permitted to fail if it is
 * actually impossible to return to a state suitable for re-initialization.
 *
 * @param force If true, attempt shutdown even if not in a sane state to do so.
 * @return 0 if shutdown was successful, -1 on failure.
 */
int ao_fini(int force);

#define AUD_OPEN_STREAM 0x01
#define AUD_OPEN_SAMPLE 0x02

/** Open a resource for audio playback.
 * The named av resource will be opened, according to the given flags.
 *
 * @param resource VFS resource name to open.
 * @param flags Flags to open with.
 * @return Opaque pointer for subsequent operations, or NULL on failure.
 */
struct audio_data *ao_open(const char *resource, int flags);

/** Close an audio resource.
 * The specified audio data will be unloaded/closed at the next possible
 * opportunity.  It is unspecified as to whether or not currently playing
 * sounds are stopped.
 *
 * @param data Opaque pointer as returned by ao_open()
 */
void ao_close(struct audio_data *data);

/** Play audio.
 * The specified audio data will be played, repeating nloops times.
 * It is unspecified as to what happens if there are currently no free playback
 * channels.
 *
 * @param data Opaque pointer as returned by ao_open().
 * @param nloops Number of times to loop, -1 to loop infinitely.
 * @param volume Scale the sample volume by this amount (between 0 and 1)
 * @return An ID representing the playing sound, or -1 on failure.
 */
int ao_play(struct audio_data *data, int nloops, double volume);

/** Stop currently playing audio.
 * The playing sound corresponding to id will be stopped at the earliest
 * possible point after this call.
 *
 * @param Playing sound ID as returned by ao_play().
 * @return 0 on success, or -1 if the ID does not correspond to a playing sound.
 */
int ao_stop(int id);

/** "Fade" a currently playing sound into a new sound.
 * Similar to stopping a sound and playing a new sound, except a smooth
 * transition is desired.  A particular plugin is allowed to do whatever
 * transition it pleases.  If id does not correspond to a currently playing
 * sound, this is equivalent to ao_play(data, nloops), except possibly with
 * a transition from silence.
 *
 * @param id Playing sound ID as returned by ao_play().
 * @param data Opaque pointer as returned by ao_open().
 * @param nloops Number of timse to loop, -1 to loop infinitely.
 * @param volume Scale the sample volume by this amount (between 0 and 1)
 * @return An ID representing the new playing sound, or -1 on failure.
 */
int ao_fade(int id, struct audio_data *data, int nloops, double volume);

/*@}*/

#endif
