/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <stdlib.h>

#include "../sys/log.h"
#include "../plugin/plugin.h"
#include "avfile.h"

static struct av_accessor *first_plugin;

static int av_plugin_register(const char *name, void *data)
{
	struct av_accessor *new = data;
	struct av_accessor *i;

	if (!new->open || !new->close)
		return -1;

	new->next = NULL;

	if (!first_plugin) {
		first_plugin = new;
	} else {
		for (i = first_plugin; i->next != NULL; i = i->next);
		i->next = new;
	}

	return 0;
}

/* TO DO : Use more than one accessor */
int av_open(const char *resource, int type,
            const struct audio_format *afmt,
	    const struct video_format *vfmt)
{
	if (!first_plugin)
		return -1;

	return first_plugin->open(resource, type, afmt, vfmt);
}

void av_close(int id)
{
	if (!first_plugin)
		return;

	return (void) first_plugin->close(id);
}

int av_read_audio(int id, void *buf, int len)
{
	if (!first_plugin)
		return -1;

	return first_plugin->read_audio(id, buf, len);
}

int av_rewind(int id)
{
	if (!first_plugin)
		return -1;
	
	return first_plugin->rewind(id);
}

static int av_access_register(const char *name, void *data)
{
	struct av_accessor *s = data;

	s->open       = av_open;
	s->close      = av_close;
	s->rewind     = av_rewind;
	s->read_audio = av_read_audio;

	return 0;
}

int av_init(void)
{
	if (plugin_addservice(AV_PLUGIN_SERVICE, av_plugin_register, NULL)) {
		log_write(LOG_ERROR, AV_MODULENAME,
		          "Failed to register service: %s", AV_PLUGIN_SERVICE);
		return -1;
	}

	if (plugin_addservice(AV_ACCESS_SERVICE, av_access_register, NULL)) {
		log_write(LOG_ERROR, AV_MODULENAME,
		          "Failed to register service: %s", AV_ACCESS_SERVICE);
		return -1;
	}

	return 0;
}
