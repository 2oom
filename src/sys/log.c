/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "log.h"
#include "../plugin/plugin.h"

/* Ring buffer for storing log info */
#define LOG_RING_SIZE 256

static char *log_ring[LOG_RING_SIZE];
static unsigned int log_ring_pos = 0;

static char *lstrings[LOG_MAX] = { "(F)", "(E)", "(W)", "(N)", "(I)", "(D)" };

enum loglevel log_display_level = LOG_DEBUG;

/* Plugin registration */
static int registerplugin(const char *name, void *data)
{
	((struct log_context *)data)->write = log_write;
	
	return 0;
}

/* Initialize logging service. */
int log_init(void)
{
	/* TO DO : Read log_display_level from config file */
	return plugin_addservice(LOG_SERVICE, registerplugin, NULL);
}

/* TO DO : Make an alternate to asprintf / vasprintf for non-GNU systems
   Probably will just truncate log entries to 512 characters or so... */
int log_write(enum loglevel level, const char *module, const char *fmt, ...)
{
	va_list ap;
	char *logmsg, *logfmt = NULL;
	int len;
	
	va_start(ap, fmt);
	if ((len = asprintf(&logfmt, "%s <%s> %s",
	                    lstrings[level%LOG_MAX], module, fmt)) == -1) {
		// failed
		goto err;
	}
	
	if ((len = vasprintf(&logmsg, logfmt, ap)) == -1) {
		// failed
		goto err;
	}
	free(logfmt);
	
	free(log_ring[log_ring_pos]);
	log_ring[log_ring_pos] = logmsg;
	
	log_ring_pos = (log_ring_pos + 1) % LOG_RING_SIZE;
	
	if (level <= log_display_level) {
		fprintf(stderr, "%s\n", logmsg);
	} else {
		fprintf(stdout, "%s\n", logmsg);
	}
	
	va_end(ap);
	
	return len;
	
err:
	va_end(ap);
	free(logfmt);
	return -1;
}
