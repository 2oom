/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif
 
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>
#include <errno.h>

#include "conf.h"
#include "log.h"

size_t conf_get_homedir(char *buf, size_t n)
{
	char *tmp;
	struct passwd *pw;
	
	/* Prioritize the user's environment, the user is always right */
	if ((tmp = getenv("HOME"))) {
		strncpy(buf, tmp, n);
		buf[n-1] = 0;
		return strlen(buf);
	}
	
	/* Only now can we fall back to passwd entries.  Some silly
	   implementations try to be smarter than the user and use only this */
	if ((pw = getpwuid(getuid()))) {
		strncpy(buf, pw->pw_dir, n);
		buf[n-1] = 0;

		setenv("HOME", pw->pw_dir, 0); /* Simplify subsequent calls */
		return strlen(buf);
	}
	
	/* We couldn't get a home dir */
	return (size_t) -1;
}

int conf_create_confdir(const char *name)
{
	struct stat buf;

	/* Attempt to create the directory... */
	if (mkdir(name, 00777) == -1) {
		if (errno != EEXIST) {
			log_write(LOG_WARNING, CONF_MODULENAME,
			          "create_confdir: mkdir(%s) failed: %s",
			          name, strerror(errno));
			return -1;
		}
	} else {
			log_write(LOG_INFO, CONF_MODULENAME,
			          "create_confdir: created directory %s",
			          name);
		return 0;
	}
	
	/* It exists, but if it's a directory that's OK. */
	if (stat(name, &buf) == -1) {
		log_write(LOG_WARNING, CONF_MODULENAME,
		          "create_confdir: stat(%s) failed: %s",
		          name, strerror(errno));
		return -1;
	}
	
	if (S_ISDIR(buf.st_mode)) {
		return 0;
	}

	log_write(LOG_WARNING, CONF_MODULENAME,
	          "conf_create_confdir: %s: not a directory",
	          name);
	return -1;
}
