/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdlib.h>

#include "loop.h"

static struct loopent {
	int id;
	struct loopent *next;
	struct loopent *prev;

	int (*func)(void);
} *loopring = NULL;

static int currentindex = 0;

int loop_addfunc(int (*callback)(void))
{
	struct loopent *new;
	
	if (!(new = malloc(sizeof(*new)))) {
		return -1;
	}

	new->id = currentindex++;
	new->func = callback;

	if (loopring) {
		new->next            = loopring;
		new->prev            = loopring->prev;
		loopring->prev->next = new;
		loopring->prev       = new;
	} else {
		new->next = new;
		new->prev = new;
	}
	
	loopring = new;
		
	return new->id;
}

int loop_delfunc(int entry)
{
	struct loopent *i = loopring;
	
	while (i != NULL) {
	
		if (i->id == entry) {
			i->next->prev = i->prev;
			i->prev->next = i->next;
			
			if (loopring == i) {
				loopring = (i->next == i) ? NULL : i->next;
			}
			
			return 0;
		}
	
		if ((i = i->next) == loopring) {
			break;
		}
	}
	
	return -1;
}

int loop_run(void)
{
	struct loopent *i;
	
	if (!loopring) {
		return -1;
	}

	for (i = loopring; i->func() == 0; i = i->next);
	
	return 0;
}
