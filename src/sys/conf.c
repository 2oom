/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "log.h"

static char *confdir = ".2oom";

/** Allocates and writes a string containing the confdir path.
 * Allocates a string of size FILENAME_MAX+1 on the heap, and then writes in
 * the path to the game configuration directory.  Caller must be sure to free()
 * the returned pointer when it is no longer needed.
 *
 * @return Pointer to allocated string, or NULL on failure.
 */
static char *getconfpath(size_t size)
{
	char *rc = malloc(size+1);
	size_t len;
	
	if (!rc) {
		return NULL;
	}
	
	if ((len = conf_get_homedir(rc, size)) == (size_t) -1) {
		return NULL;
	}

	rc[len++] = '/';
	rc[len] = 0;

	strncat(rc, confdir, size - len);
	
	return rc;
}

int conf_init(void)
{
	int conferror = 0;
	char *confpath = getconfpath(FILENAME_MAX);
	
	if (!confpath) {
		log_write(LOG_WARNING, CONF_MODULENAME,
		          "init: could not determine confpath");
		log_write(LOG_NOTICE, CONF_MODULENAME,
		          "init: configuration values will not be saved");
		          
		conferror = 1;
	} else {
		if (conf_create_confdir(confpath) == -1) {
			log_write(LOG_NOTICE, CONF_MODULENAME,
			          "init: configuration values will not be saved");
			          
			conferror = 1;
		}
	}
	
	if (conferror) {
		log_write(LOG_NOTICE, CONF_MODULENAME,
		          "init: using default configuration");
	}
	
	free(confpath);
	
	return 0;
}

char *conf_get_subpath(char *subname)
{
	size_t len;
	char *confpath = getconfpath(FILENAME_MAX+1);
	
	if (!confpath) {
		log_write(LOG_WARNING, CONF_MODULENAME,
		          "getsubpath: could not determine confpath");
		return NULL;
	}

	len = strlen(confpath);
	confpath[len++] = '/';
	confpath[len] = 0;

	return strncat(confpath, subname, FILENAME_MAX - len);
}
