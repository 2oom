/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "conf.h"
#include "log.h"
#include "../vfs/vfs.h"
#include "../plugin/plugin.h"
#include "../av/avfile.h"
#include "../av/audio.h"

int main(int argc, char **argv)
{
	/* TO DO : The program will be unoperable without most of these, do
	   something about their return values */
	log_init();
	conf_init();
	vfs_init();
	av_init();
	audio_init();
	plugin_init();

	return 0;
}
