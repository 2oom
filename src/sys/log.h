/** @file log.h
 *  Declarations for program logging facilities
*/
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef LOG_H_
#define LOG_H_

/**@defgroup Logger Logging facilities
 * @{
 */
 
/** Plugin service identifier */
#define LOG_SERVICE "LOGGER"

/** Severity level for log messages.
 * The severity of a log message is set with values from this enum, enabling,
 * for example, the user to specify that they only wish to see fatal errors
 * in their program output.  Higher values indicate lower severity.
 */
enum loglevel
{
	LOG_FATAL,     /**< Unrecoverable error, program will exit. */
	LOG_ERROR,     /**< Serious error, some functionality will be lost */
	LOG_WARNING,   /**< Minor error, functionality may be affected */
	LOG_NOTICE,    /**< No error, but could be important. */
	LOG_INFO,      /**< Information only, unimportant. */
	LOG_DEBUG,     /**< Debugging information, annoying. */
	
	LOG_MAX        /**< Maximum (least severe) value for log level. */
};

/* Externals */

/** Defines the maximum message severity that gets printed to the console */
extern enum loglevel log_display_level;

/** Initialize logging service.
 * Prepare the logging system for use, including setting up the display level,
 * log targets, registering plugin service, etc.
 *
 * @return 0 on success, -1 on failure. 
 */
int log_init(void);

/** Write an informative log message.
 * Formats the provided message and stores it in an internal ring buffer.
 * After the message is stored, this function will iterate through all log
 * targets (usually a console and/or a log file) and output messages of
 * sufficient severity to that target.
 * TO DO :: Make this function re-entrant.
 * @param level Severity of the message - higher values are less severe.
 * @param module String representing the source of the message.
 * @param fmt printf-like format string. Should not contain a trailing newline.
 * @return Length of the string written, or -1 on failure.
 */
int log_write(enum loglevel level, const char *module, const char *fmt, ...)
	__attribute__((format(printf,3,4)));
	
/**
 */
struct log_context {
	int (*write)(enum loglevel level, const char *module,
	             const char *fmt, ...);
};

	
/*@}*/

#endif
