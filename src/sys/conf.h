/** @file conf.h
 *  Declarations for program configuration facilities
*/
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
 
#ifndef CONF_H_
#define CONF_H_

#include <sys/types.h>

/** Module name for logging */
#define CONF_MODULENAME "conf"

/**@defgroup Config Configuration routines
 * @{
 */

/** Determine the user's (platform-dependent) "home" directory.
 * Attempts to determine a directory in which we can store all of our config
 * information, using whatever means exist on the target platform.
 *
 * @param buf Buffer to store the home directory in.
 * @param n Size of buffer in bytes
 * @return Length of the string (excluding null terminator), or -1 on failure.
 */
size_t conf_get_homedir(char *buf, size_t n);

/** Creates a configuration directory in a (platform-dependent) way.
 * Attempts to create the configuration directory for subsequent use by other
 * routines.  Fails if the directory could not be created or if it exists but
 * is not a directory.
 *
 * @param name Name of the directory to create
 * @see conf_get_homedir
 * @see conf_get_subpath
 * @return 0 on success, -1 on failure.
 */
int conf_create_confdir(const char *name);

/** Initialize the configuration system.
 * Prepares the environment for dealing with user config - typically involving
 * creating a directory in the user's "home" directory if it doesn't already
 * exist, and loading the config from there (or a default if not present).
 * In the event of an error involving the configuration files themselves, we
 * will attempt to recover by falling back to a default config - at which point
 * log notices will be printed indicating that a default config will be used
 * and that configuration values will not be saved.
 *
 * @return 0 on success, or -1 on unrecoverable error.
 */
int conf_init(void);

/** Allocate and write a string with a path into the config directory
 * Allocates a string of at least size FILENAME_MAX to and writes the path
 * to a subdirectory of the configuration dir.  The caller may wish to check
 * that the entire subname was written to the string, as if the confdir path
 * is too long, the string may be truncated.  The returned pointer must be
 * free()'d by the caller when it is no longer needed.
 *
 * @see conf_create_confdir
 * @param subname Subpath within the program configuration dir.
 * @return Pointer to allocated string buffer, or NULL on failure.
 */
char *conf_get_subpath(char *subname);

/*@}*/

#endif
