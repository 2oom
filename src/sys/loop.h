/** @file loop.h
 *  Declarations for program main loop
*/
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef LOOP_H_
#define LOOP_H_

/** Adds a procedure to the main loop.
 * The procedure is added to an internal list and is then called as part of the
 * program main loop.  If the callback procedure returns a non-zero value, the
 * main loop will end.
 *
 * @see loop_delfunc
 * @param callback Procedure to add to main loop.
 * @return An identifier for the added procedure, or -1 on failure.
 */
int loop_addfunc(int (*callback)(void));

/** Removes a procedure from the main loop.
 * The specified procedure which was previously added to the main loop is
 * removed from the internal list.  It will no longer be called as part of the
 * program main loop.
 *
 * @see loop_addfunc
 * @param entry Identifier for the procedure, as returned by loop_addfunc().
 * @return 0 if successful, otherwise -1.
 */
int loop_delfunc(int entry);

/** Starts the program main loop.
 * Begins executing procedures added to the internal list.  At least one
 * procedure must be added prior to calling this routine.  The loop ends when
 * one of the procedures returns a nonzero value.  Note that if no procedure
 * in the list blocks, this loop will not be friendly towards the scheduler...
 *
 * @see loop_addfunc
 * @see loop_delfunc
 * @return 0 on normal loop exit, -1 if the procedure list was empty.
 */
int loop_run(void);

#endif
