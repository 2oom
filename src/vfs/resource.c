/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif
#ifdef HAVE_STDLIB_H
#	include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#	include <string.h>
#endif
#ifdef HAVE_SEARCH_H
#	include <search.h>
#endif
#ifdef HAVE_FTW_H
#	include <ftw.h>
#endif

#include "../sys/conf.h"
#include "../sys/log.h"
#include "vfs.h"

/* Arbitrary estimated maximums */
#define FTW_ESTDEPTH 10
#define VFS_ESTFILES 3000

/* VFS Resource Table */
static struct hsearch_data vfs_table;

/* Insert a resource into the VFS */
int vfs_insert(const char *resource, const char *pathname, int module)
{
	struct vfs_resource *resdata;
	ENTRY *oldent, newent = { .key = strdup(resource) };
	char *ext;
	
	/* Strip file extension */
	if ((ext = strrchr(newent.key, '.'))) {
		*ext = 0;
	}
	
	/* Find old element */
	hsearch_r(newent, FIND, &oldent, &vfs_table);
	if (oldent) {
		free(newent.key);
		return -1;
	}

	/* Insert new element */
	/* TO DO : Keep a list of resources so that we can free this stuff */
	resdata           = malloc(sizeof(*resdata));
	resdata->pathname = strdup(pathname);
	resdata->module   = module;

	newent.data = resdata;
	if (hsearch_r(newent, ENTER, &oldent, &vfs_table) == 0) {
		free(newent.key);
		free(resdata->pathname);
		free(resdata);
		return -1;
	}
	
	return 0;
}

/* Process the result of ftw().
 * ftw_baselen must be set to the length of the search path to ftw() - allowing
 * the leading part of the path to be cut to form a resource name.
 */
static size_t ftw_baselen;
static int vfs_ftw_proc(const char *fpath, const struct stat *sb, int typeflag)
{
	if (typeflag != FTW_F) {
		return 0;
	}

	if (fpath[ftw_baselen] == '/') {
		fpath++;
	}
	
	/* TO DO : Ask plugins to process the file */
	
	/* If no plugins accept the file, insert into VFS directly */
	vfs_insert(fpath + ftw_baselen, fpath-1, -1);
	
	return 0;
}

/* Rebuild the VFS from scratch */
int vfs_rebuild(void)
{
	static int initialized = 0;
	char *tmp;

	/* (Re-)initialize VFS hash table */
	if (initialized) {
		hdestroy_r(&vfs_table);
	}
	memset(&vfs_table, 0, sizeof(vfs_table));
	if (hcreate_r(VFS_ESTFILES, &vfs_table) == 0) {
		log_write(LOG_ERROR, VFS_MODULENAME,
		          "Failed to initialize VFS hash table");
		return -1;
	}
	initialized = 1;

	/* Configure environment data search path */
	if ((tmp = getenv("TOOM_DATA"))) {
		ftw_baselen = strlen(tmp);
		ftw(tmp, vfs_ftw_proc, FTW_ESTDEPTH);
	}
	
	/* Configure user data search path */
	if (!(tmp = conf_get_subpath("data"))) {
		log_write(LOG_NOTICE, VFS_MODULENAME,
		          "init: failed to determine user data directory");
	} else {
		if (conf_create_confdir(tmp) == -1) {
			log_write(LOG_NOTICE, VFS_MODULENAME,
			          "init: ignoring user data directory");
		} else {
			ftw_baselen = strlen(tmp);
			ftw(tmp, vfs_ftw_proc, FTW_ESTDEPTH);
		}
		
		free(tmp);
	}

	/* TO DO : Configure system data search path */
	
	return 0;
}

struct vfs_resource *vfs_getresource(const char *resource)
{
	ENTRY *r, e = { .key = (char *) resource };
	hsearch_r(e, FIND, &r, &vfs_table);

	return (r) ? (r->data) : NULL;
}

