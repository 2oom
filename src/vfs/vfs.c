/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif
#ifdef HAVE_STDLIB_H
#	include <stdlib.h>
#endif

#include "../sys/log.h"
#include "../sys/conf.h"
#include "../plugin/plugin.h"
#include "vfs.h"

static int vfs_register(const char *name, void *data)
{
	log_write(LOG_NOTICE, VFS_MODULENAME, "Registered module: %s", name);
	
	return 0;
}

static int vfs_access_register(const char *name, void *data)
{
	struct vfs_access *s = data;
	s->open  = vfs_open;
	s->read  = vfs_read;
	s->seek  = vfs_seek;
	s->tell  = vfs_tell;
	s->close = vfs_close;

	log_write(LOG_DEBUG, VFS_MODULENAME, "Access registered to: %s", name);

	return 0;
}

int vfs_init(void)
{
	vfs_rebuild();

	if (plugin_addservice(VFS_SERVICE, vfs_register, NULL)) {
		log_write(LOG_ERROR, VFS_MODULENAME,
		          "Failed to register plugin service %s", VFS_SERVICE);
		return -1;
	}

	if (plugin_addservice(VFSACCESS_SERVICE, vfs_access_register, NULL)) {
		log_write(LOG_ERROR, VFS_MODULENAME,
			"Failed to register plugin service %s",
			VFSACCESS_SERVICE);
		return -1;
	}
	
	return 0;
}
