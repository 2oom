/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif
#ifdef HAVE_STDLIB_H
#	include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#	include <string.h>
#endif
#ifdef HAVE_UNISTD_H
#	include <unistd.h>
#endif
#ifdef HAVE_MMAP
#	include <sys/types.h>
#	include <sys/stat.h>
#	include <sys/mman.h>
#	include <fcntl.h>
#	include <errno.h>
#endif

#include "vfs.h"
#include "../sys/log.h"


struct vfs_file {
	int    fd;
	void  *map;
	size_t  len;
	ssize_t  offset;
};

/* Prepare a file structure with some default values */
static struct vfs_file *vfs_allocfile(void)
{
	struct vfs_file *f = malloc(sizeof(*f));
	
	if (!f) {
		return NULL;
	}
	
	f->fd = -1;
	f->len = 0;
	f->map = MAP_FAILED;
	f->offset = 0;
	
	return f;
}

/* Open a VFS resource */
struct vfs_file *vfs_open(const char *resource)
{
	struct vfs_resource *res = vfs_getresource(resource);
	struct vfs_file *f = vfs_allocfile();
	struct stat s;
	
	if (!res || !f) {
		return NULL;
	}

	/* TO DO : Something with modules rather than just failing */
	if (res->module >= 0) {
		return NULL;
	} else {
		/* Flat File */
#ifdef HAVE_MMAP
		if ((f->fd = open(res->pathname, O_RDONLY)) == -1) {
			log_write(LOG_WARNING, VFS_MODULENAME,
			          "failed to open %s: %s",
			          res->pathname, strerror(errno));
			goto err;
		}
		
		if (fstat(f->fd, &s) == -1) {
			log_write(LOG_WARNING, VFS_MODULENAME,
			          "failed to stat %s: %s",
			          res->pathname, strerror(errno));
			goto err;
		}
		
		if (s.st_size <= 0) {
			log_write(LOG_WARNING, VFS_MODULENAME,
			          "zero-length file: %s",
			          res->pathname);
			goto err;
		}
		
		f->len = s.st_size;
		
		if ((f->map = mmap(0, f->len, PROT_READ, MAP_PRIVATE,
		                   f->fd, 0)) == MAP_FAILED)
		{
			log_write(LOG_WARNING, VFS_MODULENAME,
				"failed to mmap %s: %s",
				res->pathname, strerror(errno));
			goto err;
		}

		return f;
		
#else
#	error Only mmap()ed VFS is currently supported
#endif

	}

err:
	vfs_close(f);
	
	return NULL;
}

/* Read data from a VFS resource */
ssize_t vfs_read(struct vfs_file *file, void *buf, size_t len)
{
	if (!file) {
		return -1;
	}

	if (len <= 0) {
		return -1;
	}
	
	if (file->map != MAP_FAILED) {
		if (file->offset > file->len) {
			file->offset = file->len;
		}
		
		if (file->offset + len > file->len) {
			len = file->len - file->offset;
		}
		
		memcpy(buf, file->map + file->offset, len);
		
		file->offset += len;
		
		return len;
	}
	
	return -1;
}

/* Seek in a VFS resource */
ssize_t vfs_seek(struct vfs_file *file, off_t offset, int whence)
{
	if (!file) {
		return -1;
	}

	switch (whence) {
	case SEEK_SET:
		file->offset = offset;
		break;
	case SEEK_CUR:
		file->offset += offset;
		break;
	case SEEK_END:
		file->offset = file->len + offset;
		break;
	default:
		return -1;
	}
	
	if (file->offset > file->len) {
		file->offset = file->len;
	} else if (file->offset < 0) {
		file->offset = 0;
	}
	
	return file->offset;
}

/* Get the current seek offset */
ssize_t vfs_tell(struct vfs_file *file)
{
	return file->offset;
}

/* Close a VFS resource */
void vfs_close(struct vfs_file *f)
{
	if (f->len != 0 && f->map != MAP_FAILED) {
		if (munmap(f->map, f->len) == -1) {
			log_write(LOG_WARNING, VFS_MODULENAME,
			          "failed to unmap: %s",
			          strerror(errno));
		}
	}

	if (f->fd != -1) {
		if (close(f->fd) == -1) {
			log_write(LOG_WARNING, VFS_MODULENAME,
			          "failed to close: %s",
			          strerror(errno));
		}
	}

	free(f);
}
