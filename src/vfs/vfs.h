/** @file vfs.h
 *  Declarations for the Virtual File System
*/
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef VFS_H_
#define VFS_H_

/** Module name for logging */
#define VFS_MODULENAME "VFS"

/** Plugin service identifier */
#define VFS_SERVICE "VFS"

/*
struct vfs_datasource {

}; */

/** Initialize the VFS.
 * Prepares the Virtual Filesystem layer for use by the application.  It will
 * register itself with the plugin system and perform any prologue operations
 * required.
 *
 * @return 0 on success, -1 on failure.
 */
int vfs_init(void);

/**@defgroup VFSResources VFS resource routines
 * @{
 */

struct vfs_resource {
	char *pathname;
	int module;
};

/** Rebuild the VFS resource table from scratch.
 * This completely clears the VFS resource table and rebuilds it, allowing the
 * VFS to be updated from, for example, adding a new archive plugin to the mix.
 * If this function fails, the VFS will be left in an inconsistent state and
 * should not be used (a subsequent, successful call will put the VFS back into
 * a consistent state, however).
 *
 * @see vfs_insert
 * @return 0 on success, or -1 on failure.
 */
int vfs_rebuild(void);

/** Insert a resource into the VFS.
 * Associates a reosource name with a real path and a module for accessing it.
 * When the resource is later requested, the module must be able to retrieve the
 * proper data using the provided pathname.  The special module number -1 may be
 * used to indicate that the resource can be directly accessed from disk.
 * Once a resource is inserted, it cannot be removed except by rebuilding the
 * VFS using vfs_rebuild().
 *
 * @see vfs_rebuild
 * @see vfs_open
 * @see vfs_getresource
 * @param resource The resource name to insert
 * @param pathname The physical path to associate with the resource name
 * @param module The id of the module (plugin) which can fetch the resource
 * @return 0 on success, -1 if the resource was not inserted.
 */
int vfs_insert(const char *resource, const char *pathname, int module);

/** Retrieve information about a resource.
 * Returns the information associated with a resource name, as specified by a
 * prior call to vfs_insert().
 *
 * @see vfs_insert
 * @param resource The resource name to retrieve
 * @return Pointer to the resource structure containing associated information
 */
struct vfs_resource *vfs_getresource(const char *resource);

/*@}*/

/**@defgroup VFSAccess VFS access routines
 * @{
 */

/** Service identifier for VFS access */
#define VFSACCESS_SERVICE "VFSACCESS"

/** Plugin service details */
struct vfs_access {
	struct vfs_file *(*open)(const char *); /**< See vfs_open() */
	void (*close)(struct vfs_file *); /**< See vfs_close() */
	ssize_t (*read)(struct vfs_file *, void *, size_t); /**<See vfs_read()*/
	ssize_t (*seek)(struct vfs_file *, off_t, int); /**< See vfs_seek() */
	ssize_t (*tell)(struct vfs_file *); /**< See vfs_tell() */
};

struct vfs_file;

/** Open a VFS resource.
 * Opens the specified resource by whatever means required, and returns a
 * pointer to a structure allowing further operations on the resource.
 *
 * @see vfs_read
 * @see vfs_close
 * @param resource The resource name to open
 * @return A pointer to a structure for further access, or NULL on failure
 */
struct vfs_file *vfs_open(const char *resource);

/** Read from an open VFS resource.
 * Reads at most len bytes from the current read offset in a VFS resource into
 * the buffer pointed to by buf, and increments the read offset by the number of
 * bytes read.  On error, the offset is unchanged and the contents of the buffer
 * are undefined.
 *
 * @see vfs_open
 * @see vfs_seek
 * @param file Pointer to an open VFS resource structure
 * @param buf Buffer to store output
 * @param len Maximum number of bytes to read
 * @return Number of bytes read, 0 on EOF, -1 on failure.
 */
ssize_t vfs_read(struct vfs_file *file, void *buf, size_t len);

/** Reposition read offset in an open VFS resource.
 * Repositions the read offset by adding offset bytes to the position specified
 * by whence.  If whence is set to SEEK_SET, SEEK_CUR, or SEEK_END, the offset
 * is relative to the start of the file, the current read offset, or the end of
 * the file, respectively.  Attempts to seek past the beginning (or end) of the
 * file result in seeks to the beginning (or end) of the file.
 *
 * @see vfs_open
 * @see vfs_read
 * @param file Pointer to an open VFS resource structure
 * @param offset Number of bytes to add to the position specified by whence
 * @param whence Position to start seek from
 * @return The new read offset position, or -1 on failure.
 */
ssize_t vfs_seek(struct vfs_file *file, off_t offset, int whence);

/** Get the current read offset of an open VFS resource.
 * This is semantically equivalent to vfs_seek(file, 0, SEEK_CUR).
 *
 * @see vfs_seek
 * @param file Pointer to an open VFS resource structure
 * @return The current read offset of the resource, or -1 on failure.
 */
ssize_t vfs_tell(struct vfs_file *file);

/** Close an open VFS resource.
 * Closes the open resource pointed to by f, and frees associated buffers.
 *
 * @see vfs_open
 * @param f Pointer to an open VFS resource structure
 */
void vfs_close(struct vfs_file *f);

/*@}*/

#endif
