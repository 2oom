/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "../sys/log.h"
#include "plugin.h"

static struct service {
	const char *tag;
	int (*regfunc)(const char *name, void *data);
	int (*delfunc)(const char *name, void *data);
} *services;

static unsigned int servsz = 8, servnum = 0;

static int moreservices(void)
{
	struct service *new;

	if (!(new = realloc(services, sizeof *services * servsz*2)))
		return -1;
	
	services = new;
	servsz *= 2;

	return 0;
}

/* Register a plugin service. */
int plugin_addservice(const char *tag,
                      int (*regfunc)(const char *name, void *data),
                      int (*delfunc)(const char *name, void *data))
{
	static int init;
	struct service *new;
	unsigned int i;

	/* Allocate memory, if necessary */
	if (!(init++) || servnum >= servsz)
		if (moreservices())
			return -1;

	/* Check if tag is already associated */
	for (i = 0; i < servnum; i++) {
		if (strcmp(services[i].tag, tag) == 0) {
			log_write(LOG_DEBUG, PLUGIN_MODULENAME,
			          "Duplicate service registration: %s", tag);
			return -1;
		}
	}
	
	/* Add the service */
	services[servnum].tag     = tag;
	services[servnum].regfunc = regfunc;
	services[servnum].delfunc = delfunc;
	servnum++;
	
	log_write(LOG_INFO, PLUGIN_MODULENAME,
	          "Service registered: %s", tag);
	
	return 0;
}

/* Unregister a plugin service. */
/* TO DO: This surely is broken, fix it. */
int plugin_delservice(const char *tag)
{
	unsigned int i;

	for (i = 0; i < servnum; i++) {
		if (strcmp(services[i].tag, tag) == 0) {
			memcpy(&services[i], &services[servnum-1],
			       sizeof(*services));
			servnum--;
			return 0;
		}
	}
	
	return -1;
}

/* Extend a plugin service. */
int plugin_useservice(const char *tag, const char *name, void *data)
{
	unsigned int i;
	
	for (i = 0; i < servnum; i++) {
		if (strcmp(services[i].tag, tag) == 0) {
			return services[i].regfunc(name, data);
		}
	}
	
	return -1;
}

/* Remove extension from a service. */
int plugin_shunservice(const char *tag, const char *name, void *data)
{
	unsigned int i;

	for (i = 0; i < servnum; i++) {
		if (strcmp(services[i].tag, tag) == 0) {
			if (services[i].delfunc) {
				return services[i].delfunc(name, data);
			} else {
				return 0;
			}
		}
	}
	
	return -1;
}
