/** @file plugin.h
 *  Declarations for the core plugin system
*/
/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifndef PLUGIN_H_
#define PLUGIN_H_

/** Module name for logging */
#define PLUGIN_MODULENAME "plugin"

/**@defgroup PluginCore Plugin core system routines
 * @{
 */
 
/** Initialize the plugin system.
 * Performs any prologue required for dynamically loading plugins, then finds
 * plugins to open by searching system and/or user directories for them.
 * A plugin is considered loaded if the dynamic linker succeeded, whether any
 * plugins have registered with other subsystems such as the VFS is left to
 * the caller to determine.
 *
 * @see plugin_exit
 * @return The number of plugins loaded, or -1 on error.
 */
int plugin_init(void);

/** Shut down the plugin system.
 * Unloads all plugins and then shuts down the plugin system.  Note that all
 * subsystems which use plugins must be shut down prior to shutting down
 * plugins - otherwise this call will fail and nothing will be unloaded.
 *
 * @return 0 on successful shutdown, -1 on error.
 */
int plugin_exit(void);

/** Load a plugin.
 * Attempts to load the specified plugin.  Succeeds if the plugin was
 * successfully loaded into memory - success does not indicate that the
 * plugin has actually registered with any subsystem such as VFS.  The system
 * must first be initialized by a call to plugin_init() before using this
 * routine.
 *
 * @see plugin_init
 * @param name The name to attempt to load
 * @return 0 on sucess, -1 on failure.
 */
int plugin_open(const char *name);

/*@}*/

/**@defgroup PluginServices Plugin service routines
 * @{
 */
 
/** plugin_useservice pointer type */
typedef int(*plugin_useservice_t)(const char *, const char *, void *);

/** Register a plugin service.
 * Associates the given tag with a registration function.  Once the service
 * is registered, plugins may be used to extend the functionality of that
 * service.  Note that, when possible, plugin services should be registered
 * prior to the call to plugin_init(), as this will allow the automatic loading
 * to work properly.  NOTE: tag must not be modified or freed after registering
 * a service.  It is recommended to use a constant string.
 *
 * @see plugin_delservice
 * @see plugin_init
 * @param tag Unique string identifier for this service
 * @param regfunc Function to call on plugin registration
 * @param delfunc Function to call on plugin unregistration
 * @return 0 on success, -1 if the service could not be registered
 */
int plugin_addservice(const char *tag,
                      int (*regfunc)(const char *name, void *data),
                      int (*delfunc)(const char *name, void *data));

/** Unregister a plugin service.
 * Disassociates the given tag with a registration function.  New plugins which
 * attempt to use the old tag will fail.  IMPORTANT: This does not guarantee
 * that plugins which use a service are unloaded!  This merely prevents new
 * plugins from being registered with the service.
 *
 * @see plugin_addservice
 * @param tag String identifier previously registered with plugin_addservice()
 * @return 0 on success, -1 if the service could not be unregistered.
 */
int plugin_delservice(const char *tag);

/** Extend a plugin service.
 * Provides the given registration data to the service associated with tag,
 * extending the service's functionality.  The details of the data to be passed
 * to a service depends on that service, usually a struct containing function
 * pointers and/or options.
 *
 * @see plugin_shunservice
 * @see plugin_addservice
 * @param tag String identifer previously registered with plugin_addservice()
 * @param name String identifier for the module to be loaded
 * @param data Service-specific data structure of registration data
 * @return The return value of the registration function, -1 on failure.
 */
int plugin_useservice(const char *tag, const char *name, void *data);

/** Remove extension from a service.
 * Provides the given unregistration data to the service associated with tag,
 * removing functionality from that service.
 *
 * @see plugin_useservice
 * @see plugin_addservice
 * @param tag String identifer previously registered with plugin_addservice()
 * @param name String identifier for the module to be unloaded
 * @param data Service-specific data structure of unregistration data
 * @return The return value of the registration function, -1 on failure.
 */
int plugin_shunservice(const char *tag, const char *name, void *data);

/*@}*/

#endif
