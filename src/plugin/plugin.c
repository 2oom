/*  2ooM: The Master of Orion II Reverse Engineering Project
 *  Copyright (C) 2006 Nick Bowler
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdlib.h>

#include <ltdl.h>

#include "plugin.h"
#include "../sys/log.h"
#include "../sys/conf.h"

static int initialized = 0;

/* Open the specified plugin */
int plugin_open(const char *name)
{
	lt_dlhandle h;
	lt_ptr init;
	
	if (!initialized) {
		log_write(LOG_DEBUG, PLUGIN_MODULENAME,
		          "open: called while uninitialized");
		return -1;
	}
	
	if (!(h = lt_dlopenext(name))) {
		log_write(LOG_DEBUG, PLUGIN_MODULENAME,
		          "open: %s", lt_dlerror());
		return -1;
	}
	
	if ((init = lt_dlsym(h, "init"))) {
		if (((int (*)(plugin_useservice_t))init)(plugin_useservice)) {
			log_write(LOG_WARNING, PLUGIN_MODULENAME,
			          "open: failed initialization of %s", name);
			return -1;
		}
	} else {
		log_write(LOG_WARNING, PLUGIN_MODULENAME,
		          "init: no init symbol in %s", name);
		return -1;
	}
	
	log_write(LOG_DEBUG, PLUGIN_MODULENAME,
	          "open: loaded plugin %s", name);

	return 0;
}

/* lt_dlforeachfile callback */
static int plugadd(const char *filename, lt_ptr data)
{
	/* TO DO : Make this not load the same plugin from different places */
	plugin_open(filename);
	return 0;
}

/* Initialize plugin system */
int plugin_init(void)
{
	char *tmp;
	
	if (initialized) {
		log_write(LOG_DEBUG, PLUGIN_MODULENAME,
		          "init: called while already initialized");
		return -1;
	}
	
	/* Setup dynamic loader */
	if (lt_dlinit()) {
		while ((tmp = (char *) lt_dlerror())) {
			log_write(LOG_ERROR, PLUGIN_MODULENAME,
			          "init: failed to initialize dynamic loader: %s",
			          tmp);
		}
		
		return -1;
	}
	
	initialized = 1;
	
	/* Configure environment plugin search path */
	if ((tmp = getenv("TOOM_PLUGINS"))) {
		lt_dlforeachfile(tmp, plugadd, NULL);
	}
	
	/* Configure user plugin search path */
	if (!(tmp = conf_get_subpath("plugins"))) {
		log_write(LOG_NOTICE, PLUGIN_MODULENAME,
		          "init: failed to determine user plugin directory");
	} else {
		if (conf_create_confdir(tmp) == -1) {
			log_write(LOG_NOTICE, PLUGIN_MODULENAME,
			          "init: ignoring user plugin directory");
		} else {
			/* Load user plugins */
			lt_dlforeachfile(tmp, plugadd, NULL);
		}
		
		free(tmp);
	}
	
	/* TO DO : Configure system plugin search path */
	
	return 0;
}

int plugin_exit(void)
{
	if (!initialized) {
		log_write(LOG_DEBUG, PLUGIN_MODULENAME,
		          "exit: called while uninitialized");
		return -1;
	}

	return 0;
}
