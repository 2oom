#!/bin/sh

# For debugging
export CFLAGS="-W -Wall -pedantic -O0 -ggdb"

die() {
    echo $1
    exit 1
}

aclocal                || die "Failed to run aclocal"
autoheader             || die "Failed to run autoheader"
libtoolize --force     || die "Failed to run libtoolize"
automake --add-missing || die "Failed to run automake"
autoconf               || die "Failed to run autoconf"

./configure -C "$@"
