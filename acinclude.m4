dnl Borrowed from Transcode (http://www.transcoding.org/cgi-bin/transcode)

dnl PKG_INIT(rptfile, errfile)
dnl
AC_DEFUN([PKG_INIT],
[
pkg_err="no"
pkg_err_file="pkg_err_file"
pkg_rpt_file="pkg_rpt_file"

if test x"$1" != x"" ; then
  pkg_rpt_file="$1"
fi
echo -n > $pkg_rpt_file

if test x"$2" != x"" ; then
  pkg_err_file="$2"
fi
echo -n > $pkg_err_file
])


dnl PKG_ERROR(name, object, req-enable, pkg, url, [error message])
AC_DEFUN([PKG_ERROR],
[
pkg_err="yes"
this_pkg_err="yes"

prob=""
if test x"$3" = x"required" ; then
  prob="requirement failed"
else
  prob="option '--enable-$1' failed"
fi

cat >> $pkg_err_file <<EOF
ERROR: $prob: $6
$2 can be found in the following packages:
  $4  $5

EOF
])


dnl PKG_CHECK(pkg-name, def-enabled, var-name, pkgconfig-name, conf-script,
dnl     header, lib, symbol)
dnl Test for pkg-name, and define var-name_CFLAGS and var-name_LIBS
dnl   and HAVE_var-name if found
dnl
dnl 1 name          name of package; required (pkg-config name if applicable)
dnl 2 req-enable    enable  by default, 'required', 'yes' or 'no'; required
dnl 3 var-name      name stub for variables, preferably uppercase; required
dnl 4 conf-script   name of "-config" script or 'no'
dnl 5 header        header file to check or 'none'
dnl 6 lib           library to check or 'none'
dnl 7 symbol        symbol from the library to check or ''
dnl 8 pkg           package (pkg-config name if applicable)
dnl 9 url           homepage for the package

AC_DEFUN([PKG_CHECK],
[
if test x"$2" != x"required" ; then
  AC_MSG_CHECKING([whether $1 support is requested])
  AC_ARG_ENABLE($1,
    AC_HELP_STRING([--enable-$1],
      [build with $1 support ($2)]),
    [case "${enableval}" in
      yes) ;;
      no)  ;;
      *) AC_MSG_ERROR(bad value ${enableval} for --enable-$1) ;;
    esac],
    enable_$1="$2")
  AC_MSG_RESULT($enable_$1)
else
  enable_$1="yes"
fi

AC_ARG_WITH($1-prefix,
  AC_HELP_STRING([--with-$1-prefix=PFX],
    [prefix where $1 is installed (/usr)]),
  w_$1_p="$withval", w_$1_p="")

AC_ARG_WITH($1-includes,
  AC_HELP_STRING([--with-$1-includes=DIR],
    [directory where $1 headers ($5) are installed (/usr/include)]),
  w_$1_i="$withval", w_$1_i="")

AC_ARG_WITH($1-libs,
  AC_HELP_STRING([--with-$1-libs=DIR],
    [directory where $1 libararies (lib$6.so) are installed (/usr/lib)]),
  w_$1_l="$withval", w_$1_l="")

have_$1="no"
this_pkg_err="no"

if test x"$enable_$1" = x"yes" ; then

  dnl pkg-config

  pkg_config_$1="no"
  AC_MSG_CHECKING([for pkgconfig support for $1])
  if test x"$PKG_CONFIG" != x"no" ; then
    if $PKG_CONFIG $8 --exists ; then
      pkg_config_$1="yes"
    fi
  fi
  AC_MSG_RESULT($pkg_config_$1)

  dnl *-config

  if test x"$4" != x"no" ; then
    if test x"$w_$1_p" != x"" ; then
      if test -x $w_$1_p/bin/$4 ; then
        $1_config="$w_$1_p/bin/$4"
      fi
    fi
    AC_PATH_PROG($1_config, $4, no)
  else
    $1_config="no"
  fi

  # get and test the _CFLAGS

  AC_MSG_CHECKING([how to determine $3_CFLAGS])
  if test x"$w_$1_i" != x"" ; then
    $1_ii="-I$w_$1_i"
    AC_MSG_RESULT(user)
  else
    if test x"$pkg_config_$1" != x"no" ; then
      $1_ii="`$PKG_CONFIG $8 --cflags`"
      AC_MSG_RESULT(pkg-config)
    else
      if test x"$$1_config" != x"no" ; then
        $1_ii="`$$1_config --cflags`"
        AC_MSG_RESULT($$1_config)
      else
        if test x"$w_$1_p" != x"" ; then
          $1_ii="-I$w_$1_p/include"
          AC_MSG_RESULT(prefix)
        else
          $1_ii="-I/usr/include"
          AC_MSG_RESULT(default)
        fi
      fi
    fi
  fi
  ipaths="" ; xi=""
  for i in $$1_ii ; do
    case $i in
      -I*) ipaths="$ipaths $i" ;;
        *) xi="$xi $i" ;;
    esac
  done
  $1_ii="$ipaths"
  $1_ii="`echo $$1_ii | sed -e 's/  */ /g'`"
  $3_EXTRA_CFLAGS="$$3_EXTRA_CFLAGS $xi"
  $3_EXTRA_CFLAGS="`echo $$3_EXTRA_CFLAGS | sed -e 's/  */ /g'`"

  if test x"$5" != x"none" ; then
    save_CPPFLAGS="$CPPFLAGS"
    CPPFLAGS="$CPPFLAGS $$1_ii"
    AC_CHECK_HEADER([$5],
      [$3_CFLAGS="$$1_ii"],
      [PKG_ERROR($1, $5, $2, $8, $9, [cannot compile $5])])
    CPPFLAGS="$save_CPPFLAGS"
  fi

  # get and test the _LIBS

  AC_MSG_CHECKING([how to determine $3_LIBS])
  if test x"$w_$1_l" != x"" ; then
    $1_ll="-L$w_$1_l"
    AC_MSG_RESULT(user)
  else
    if test x"$pkg_config_$1" != x"no" ; then
      $1_ll="`$PKG_CONFIG $8 --libs`"
      AC_MSG_RESULT(pkg-config)
    else
      if test x"$$1_config" != x"no" ; then
        $1_ll="`$$1_config --libs`"
        AC_MSG_RESULT($$1_config)
      else
        if test x"$w_$1_p" != x"" ; then
          $1_ll="-L$w_$1_p${deflib}"
          AC_MSG_RESULT(prefix)
        else
          $1_ll="-L/usr${deflib}"
          AC_MSG_RESULT(default)
        fi
      fi
    fi
  fi
  lpaths="" ; xlibs="" ; xlf=""
  for l in $$1_ll ; do
    case $l in
      -L*) lpaths="$lpaths $l" ;;
      -l*) test x"$l" != x"-l$6" && xlibs="$xlibs $l" ;;
        *) xlf="$xlf $l" ;;
    esac
  done
  $1_ll="$lpaths"
  $1_ll="`echo $$1_ll | sed -e 's/  */ /g'`"
  xl=""
  for i in $xlibs $xlf ; do
    echo " $$3_EXTRA_LIBS " | grep -vq " $i " && xl="$xl $i"
  done
  $3_EXTRA_LIBS="$$3_EXTRA_LIBS $xl"
  $3_EXTRA_LIBS="`echo $$3_EXTRA_LIBS | sed -e 's/  */ /g'`"

  if test x"$6" != x"none" ; then
    save_LDFLAGS="$LDFLAGS"
    LDFLAGS="$LDFLAGS $$1_ll"
    AC_CHECK_LIB([$6], [$7],
      [$3_LIBS="$$1_ll -l$6 $$3_EXTRA_LIBS"],
      [PKG_ERROR($1, lib$6, $2, $8, $9, [cannot link against lib$6])],
      [$$3_EXTRA_LIBS])
    LDFLAGS="$save_LDFLAGS"
  fi

  if test x"$this_pkg_err" = x"no" ; then
    have_$1="yes"
  fi

else
  $3_CFLAGS=""
  $3_LIBS=""  
fi
])

